package com.drohobytskyy.annotations.Controller;

import com.drohobytskyy.annotations.Model.CallingClass;
import com.drohobytskyy.annotations.Model.MyGenericClass;
import com.drohobytskyy.annotations.Model.TestClass;
import com.drohobytskyy.annotations.View.ConsoleView;

import java.io.IOException;

public class Controller {
    private ConsoleView view;
    private TestClass testClass;
    private CallingClass callingClass;
    private final static String DIVIDER = "----------------------------------------------------------------------" +
            "-----------------------------------------------------------------";
    public Controller(ConsoleView view, TestClass testClass, CallingClass callingClass) throws IOException {
        this.view = view;
        view.controller = this;
        this.testClass = testClass;
        this.callingClass = callingClass;
        testClass.view = view;
        callingClass.view = view;
        callingClass.testClass = testClass;
        callingClass.execute();
        view.createMenu();
        view.executeMenu(view.menu);
    }

    public void printFieldsWithAnnotashka() {
        view.logger.warn(DIVIDER);
        callingClass.printFieldsWithAnnotashka();
    }

    public void printValuesOfAnnotashka() {
        view.logger.warn(DIVIDER);
        callingClass.printValuesOfAnnotashka();
    }

    public void invokeThreeMethods() {
        view.logger.warn(DIVIDER);
        callingClass.invokeThreeMethods();
    }

    public void setValueNotKnowingType() {
        view.logger.warn(DIVIDER);
        callingClass.setValueNotKnowingType();
    }

    public void invokeMethodsWithStringArgs() {
        view.logger.warn(DIVIDER);
        callingClass.invokeMethodsWithStringArgs();
    }

    public void showInformationAboutClass() {
        view.logger.warn(DIVIDER);
        MyGenericClass myGenericClass = new MyGenericClass(TestClass.class, view);
    }
}
