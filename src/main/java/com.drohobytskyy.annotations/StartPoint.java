package com.drohobytskyy.annotations;

import com.drohobytskyy.annotations.Controller.Controller;
import com.drohobytskyy.annotations.Model.CallingClass;
import com.drohobytskyy.annotations.Model.TestClass;
import com.drohobytskyy.annotations.View.ConsoleView;

import java.io.IOException;

public class StartPoint {
    public static void main(String[] args) {
        try {
            new Controller(new ConsoleView(), new TestClass(), new CallingClass());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
