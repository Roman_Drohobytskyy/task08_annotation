package com.drohobytskyy.annotations.View;

import com.drohobytskyy.annotations.Controller.Controller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedHashMap;
import java.util.Map;

public class ConsoleView {
    public final static String EXIT = "exit";
    private BufferedReader input;
    public Controller controller;
    public Map<String, MenuItem> menu;
    public Logger logger = LogManager.getLogger(ConsoleView.class);

    public ConsoleView() throws IOException {
        input = new BufferedReader(new InputStreamReader(System.in));
    }

    public void createMenu() {
        menu = new LinkedHashMap<>();
        menu.put("1", new MenuItem(" 1   - Through reflection print  fields in the class " +
                "that were annotate by your annotation.",
                controller::printFieldsWithAnnotashka));
        menu.put("2", new MenuItem(" 2   - Print annotation value into console.",
                controller::printValuesOfAnnotashka));
        menu.put("3", new MenuItem(" 3   - Invoke method (three methods with different parameters and " +
                "return types).",
                controller::invokeThreeMethods));
        menu.put("4", new MenuItem(" 4   - Set value into field not knowing its type.",
                controller::setValueNotKnowingType));
        menu.put("5", new MenuItem(" 5   - Invoke myMethod(String a, int ... args) " +
                "and myMethod(String … args).",
                controller::invokeMethodsWithStringArgs));
        menu.put("6", new MenuItem(" 6   - Create your own class that received object of unknown type and " +
                "show all information about that Class.",
                controller::showInformationAboutClass));
        menu.put(EXIT, new MenuItem(EXIT + " - exit from app", this::exitFromMenu));
    }

    public void executeMenu(Map<String, MenuItem> menu) throws IOException {
        String keyMenu;
        do {
            logger.warn("-------------------------------------------------------------------------------------"
                    + "-------------------------------------------------");

            logger.warn("Please, select menu point.");
            outputMenu(menu);
            keyMenu = input.readLine().toLowerCase();
            try {
                menu.get(keyMenu).getLink().print();
            } catch (Exception e) {
                logger.error("Invalid input! Please try again.");
            }
        } while (true);
    }

    private void outputMenu(Map<String, MenuItem> menu) {
        for (MenuItem pair : menu.values()) {
            logger.info(pair.getDescription());
        }
    }

    public void exitFromMenu() {
        logger.warn("Have an amazing day!");
        System.exit(0);
    }

}

