package com.drohobytskyy.annotations.View;

import java.io.IOException;

@FunctionalInterface
public interface Printable {

    void print() throws IOException;
}

