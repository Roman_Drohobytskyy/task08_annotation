package com.drohobytskyy.annotations.Model;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(value ={ElementType.FIELD})
public @interface Annotashka {
    int age() default 18;
    String name() default "noName";
}
