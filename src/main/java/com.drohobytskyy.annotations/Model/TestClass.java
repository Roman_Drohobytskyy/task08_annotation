package com.drohobytskyy.annotations.Model;

import com.drohobytskyy.annotations.View.ConsoleView;

import java.util.Arrays;

public class TestClass {
    public ConsoleView view;
    @Annotashka(age = 21)
    private int value1 = 333;
    @Annotashka(name = "Peter")
    private int value2 = 777;
    private int value3 = 999;

    public void method1() {
        view.logger.info("Method 1 is working...");
    }

    private void method2(String text) {
        view.logger.info("Method 2 is working: " + text);
    }

    private String method3(int a, int b, String s) {
        view.logger.info("Method 3 is working");
        return s + " " + a * b;
    }

    public void myMethod(String a, int ... args) {
        view.logger.info(a + Arrays.stream(args).reduce((s1, s2) -> s1 + s2).orElse(0));
    }

    private void myMethod(String[] args) {
        Arrays.stream(args).forEach(arg -> view.logger.info(arg + " "));
    }

}
