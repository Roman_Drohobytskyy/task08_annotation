package com.drohobytskyy.annotations.Model;

import com.drohobytskyy.annotations.View.ConsoleView;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class CallingClass {
    public ConsoleView view;
    public TestClass testClass;
    Class clazz;

    public void execute() {
        clazz = testClass.getClass();
    }

    public static void main(String[] args) {
        TestClass testClass = new TestClass();
        CallingClass cl = new CallingClass();
        cl.invokeMethodsWithStringArgs();
    }

    public void printFieldsWithAnnotashka() {
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            if (field.isAnnotationPresent(Annotashka.class)) {
                view.logger.info(field);
            }
        }
    }

    public void printValuesOfAnnotashka() {
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            if (field.isAnnotationPresent(Annotashka.class)) {
                view.logger.info(field.getDeclaredAnnotation(Annotashka.class));
            }
        }
    }

    public void invokeThreeMethods() {
        Method[] methods = clazz.getDeclaredMethods();
        for (Method method : methods) {
            view.logger.info(method);
        }
        try {//first method
            Method method = clazz.getDeclaredMethod("method1");
            method.invoke(testClass);
            // second method
            Class[] paramTypes2 = new Class[]{String.class};
            Method method2 = clazz.getDeclaredMethod("method2", paramTypes2);
            method2.setAccessible(true);
            Object[] args = new Object[]{"my text"};
            method2.invoke(testClass, args);
            //third method
            Method method3 = clazz.getDeclaredMethod("method3", new Class[]{int.class, int.class, String.class});
            method3.setAccessible(true);
            view.logger.info(method3.invoke(testClass, 2, 3, "Met.3 Product = "));
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public void invokeMethodsWithStringArgs() {
        try {//String...
            view.logger.warn(" String[].class");
            Class[] argTypes1 = new Class[] { String[].class };
            Method myMethodStrings = clazz.getDeclaredMethod("myMethod", argTypes1);
            myMethodStrings.setAccessible(true);
            String[] mainArgs1 = new String[]{"Hello","world","!"};
            myMethodStrings.invoke(testClass, (Object)mainArgs1);
            //String, int ...
            view.logger.warn(" String.class, int[].class ");
            Class[] argTypes2 = new Class[] {String.class, int[].class };
            Method myMethodStringInts = clazz.getDeclaredMethod("myMethod", argTypes2);
            myMethodStringInts.setAccessible(true);
            int[] mainArgs = new int[]{1, 2, 3, 4, 5};
            myMethodStringInts.invoke(testClass,"Sum of numbers: ", (Object)mainArgs);

        } catch (NoSuchMethodException x) {
            x.printStackTrace();
        } catch (IllegalAccessException x) {
            x.printStackTrace();
        } catch (InvocationTargetException x) {
            x.printStackTrace();
        }

    }

    public void setValueNotKnowingType() {
        try {
            Field field = clazz.getDeclaredField("value3");
            field.setAccessible(true);
            view.logger.info( "Type: " + field.getType() + ", value = " + field.get(testClass));
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
        try {
            Field field = clazz.getDeclaredField("value3");
            field.setAccessible(true);
            field.setInt(testClass, 555);
            view.logger.info("New value = "+ field.get(testClass));
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}
