package com.drohobytskyy.annotations.Model;

import com.drohobytskyy.annotations.View.ConsoleView;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

public class MyGenericClass<T> {
    ConsoleView view;

    private T obj;

// Compile Error!
//  public MyGenericClass() {
//    this.obj = new T();
//  }

    private final Class<T> clazz;

    public MyGenericClass(Class<T> clazz, ConsoleView view) {
        this.clazz = clazz;
        try {
            //obj = clazz.newInstance();

            // 1. Class.newInstance() can only invoke the zero-argument constructor,
            // while Constructor.newInstance() may invoke any constructor, regardless of the number of parameters.

            // 2. Class.newInstance() requires that the constructor be visible;
            // Constructor.newInstance() may invoke private constructors under certain circumstances.
            obj = clazz.getConstructor().newInstance();

            view.logger.warn("The declared fields of class " + clazz.getSimpleName() + " are : ");
            Field[] fields = clazz.getDeclaredFields();
            // Printing field names
            for (Field field : fields) {
                view.logger.info("  " + field.getName() + " (" + field.getType() + ")");
            }

        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
    }
}

